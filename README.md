---

[![Netlify Status](https://api.netlify.com/api/v1/badges/0d0ab584-60c3-4fc0-a52b-f5b7b4211b9a/deploy-status)](https://app.netlify.com/sites/xcl-app/deploys)

---

## What is XCL-App ?

No matter the libraries or frameworks that you use, with XCL you build in fact a **content management system**, a **web app** or a **desktop app**.

XCL runs on top of the simple Cube core:

* class abstraction model,
* design paterns,
* object oriented programming.


## What can I do with XCL-App Design ?

#### Design your web application platform with ease

XCL is framework agnostic, so developers and designers have a free choice of libraries and css and javascript frameworks.

#### Code the behavior of your system

Once you have created your content, XCL generates the templates of all your modules. You only have then to implement a custom Theme (html, css, js).

XCL provides a Graphical User Interface Wizard to manage your modules. You can easily navigate threw admin dashboard to manage modules and users permissions, create blocks of content with HTML or PHP and customize the user interface and user experience of your application.

#### Create content graphically

There is no need to code to code to create content. XCL provides WYSIWYG module AND A file manager that can mount cloud drives.

Create a page in XCL is like creating a document in Word or Power Point.

#### Run your App

You can run XCL on Apache or Nginx, MySQL or Maria and PHP7 (Linux, macOS, Windows).

#### Debug your system

XCL includes a debug system for PHP, SQL and the render engine. You can turn it off from the dashboard general settings.

All modifications to the main Theme or moddules templates done with XCL editor will clear the cache to reload your modifications.

## Build

#### Installation

Clone the repository:

```sh
git clone https://github.com/xoopscube/xcl.git
```

Once you have cloned the repository, you don't have to install dependencies, just browse to localhost or web domain and follow the GUI Wizard steps:

```sh
- http://127.0.0.1
- https://localhost.name
- https://domainename.app
```	 	

#### Build for web

Here are the different tasks you can use to start XCL:

```sh
chmod 755 directory cahe
chmod 755 directory uploads
chmod 755 directory templates
```

Then you can start your server:

Once server started, go to [http://localhost/](http://localhost).

#### Build desktop with Electron

Clone or download the default xcl-desktop

To build for [Electron](http://electron.atom.io):

```sh
npm run electron
```

Copy the content of `/dist` directory [XCL-App for Electron](https://github.com/xoopscube/xcl-desktop-electron) project.

Then in your [XCL-desktop for Electron](https://github.com/design-first/system-designer-electron) project:

```sh
npm run start
```

## Development

#### Web

To start XCL in development mode for web:

```sh
go to admin dashboard
general settings
activate debug
```

Once server started, go to [http://localhost/](http://localhost). 

All the modifications to the source code of XCL will output a report to help you solve any issue on the page.

## Documentation

* [Quick Start](https://github.com/xoopscube/documentation/quick-start)
* [Documentation](https://github.com/xoopscube/documentation/)

## Community

* [Code of Conduct](CODE_OF_CONDUCT.md)
* [Contributing Guidelines](CONTRIBUTING.md)
